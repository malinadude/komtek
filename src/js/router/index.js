import Vue from 'vue';
import Router from 'vue-router';

// Patient
import PatientSingle from '../views/patient/Single';
import PatientList from '../views/patient/List';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        // Patient
        {
            name: 'PatientListView',
            path: '/',
            component: PatientList,
        },
        {
            name: 'PatientView',
            path: '/patient/id/:patientId',
            component: PatientSingle,
        },
        {
            name: 'PatientCreate',
            path: '/patient/create',
            component: PatientSingle,
        },
        {
            name: 'PatientEdit',
            path: '/patient/id/:patientId',
            component: PatientSingle,
        },
    ],
});
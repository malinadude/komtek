import axios from 'axios';
import qs from 'query-string';

let actions = {
    setTableType({commit}, type) {
        commit('SET_TABLE_TYPE', type);
    },
    setTableHeadings({commit}, tableHeadings) {
        commit('SET_TABLE_HEADINGS', tableHeadings);
    },
    tableColumnDisplay({commit}, column) {
        commit('SET_TABLE_COLUMN_DISPLAY', column);
    },

    // Patient
    getListPatient({commit}) {
        if (!localStorage.getItem("patients")) {
            localStorage['patients'] = JSON.stringify([
                {
                    id: 0,
                    surname: 'Иванов',
                    name: 'Иван',
                    patronymic: 'Иванович',
                    birthday: 168307200,
                    sex: 'Мужской',
                    snils: '159-858-697-58',
                },
                {
                    id: 1,
                    surname: 'Станиславов',
                    name: 'Станислав',
                    patronymic: 'Станиславович',
                    birthday: 593568000,
                    sex: 'Мужской',
                    snils: '159-858-697-58',
                },
                {
                    id: 2,
                    surname: 'Сергеев',
                    name: 'Сергей',
                    patronymic: 'Сергеевич',
                    birthday: 438307200,
                    sex: 'Женский',
                    snils: '159-858-697-58',
                },
            ])
        }

        commit('GET_LIST_PATIENT', JSON.parse(localStorage.getItem("patients")));
    },
    getPatient({commit, getters}, patientId) {
        let patients = JSON.parse(localStorage.getItem("patients"));

        patients.forEach(patient => {
            if (patient.id === Number(patientId)) {
                commit('GET_PATIENT', patient);
            }
        });
    },
    createPatient({commit, state}, patient) {
        let patients = JSON.parse(localStorage.getItem("patients"));
        patient['id'] = patients.length ? patients[patients.length - 1].id + 1 : 0;

        patients.push(patient);
        localStorage['patients'] = JSON.stringify(patients);

        commit('CREATE_PATIENT', patient);
        commit('GET_PATIENT', patient);
    },
    editPatient({commit, state}, patient) {
        let patients = JSON.parse(localStorage.getItem("patients"));

        patients.forEach((patientItem, index) => {
            if (patientItem.id === patient.id) {
                patients[index] = patient;
                localStorage['patients'] = JSON.stringify(patients);
                commit('GET_LIST_PATIENT', patients);
            }
        });

        commit('GET_PATIENT', patient);
    },
    deletePatient({commit, state, dispatch}, patientId) {
        let patients = JSON.parse(localStorage.getItem("patients"));

        patients.forEach((patientItem, index) => {
            if (patientItem.id === patientId) {
                patients.splice(index, 1);
                dispatch('deleteConsultationsPatient', patientId);
            }
        });

        localStorage['patients'] = JSON.stringify(patients);
        commit('GET_LIST_PATIENT', patients);
    },

    // Consultation
    getConsultations({commit}) {
        if (!localStorage.getItem("consultations")) {
            localStorage['consultations'] = JSON.stringify([
                {
                    id: 0,
                    patient: 0,
                    date: 168307200,
                    time: '12:13',
                    symptoms: 'головокружение, тошнота',
                },
                {
                    id: 1,
                    patient: 1,
                    date: 168307200,
                    time: '11:34',
                    symptoms: 'головокружение, тошнота',
                },
                {
                    id: 2,
                    patient: 2,
                    date: 168307200,
                    time: '14:13',
                    symptoms: 'головокружение, тошнота',
                },
                {
                    id: 3,
                    patient: 1,
                    date: 168307200,
                    time: '17:13',
                    symptoms: 'головокружение, тошнота',
                },
            ])
        }

        commit('GET_CONSULTATIONS', JSON.parse(localStorage.getItem("consultations")));
    },
    getConsultationsPatient({commit, dispatch}, patientId) {
        let consultations = JSON.parse(localStorage.getItem("consultations"));
        let consultationsPatient = [];

        consultations.forEach(consultation => {
            if (Number(consultation.patient) === Number(patientId)) {
                consultationsPatient.push(consultation)
            }
        });

        commit('GET_CONSULTATIONS_PATIENT', consultationsPatient);
    },
    deleteConsultationsPatient({commit}, patientId) {
        let consultations = JSON.parse(localStorage.getItem("consultations"));
        let consultationsId = [];

        consultations.forEach((consultationItem, index) => {
            if (Number(consultationItem.patient) === Number(patientId)) {
                consultationsId.push(index);
            }
        });

        consultations = consultations.filter(function(e, i){
            return consultationsId.indexOf(i) < 0;
        });

        localStorage['consultations'] = JSON.stringify(consultations);
        commit('GET_CONSULTATIONS', JSON.parse(localStorage.getItem("consultations")));
    },
    getConsultation({commit}, consultationId = null) {
        if (consultationId !== null) {
            let consultations = JSON.parse(localStorage.getItem("consultations"));

            consultations.forEach(consultation => {
                if (Number(consultation.id) === Number(consultationId)) {
                    commit('GET_CONSULTATION', consultation);
                }
            });
        } else {
            commit('GET_CONSULTATION', null);
        }
    },
    createConsultation({commit, state}, consultation) {
        let consultations = JSON.parse(localStorage.getItem("consultations"));
        consultation['id'] = consultations.length ? consultations[consultations.length - 1].id + 1 : 0;

        consultations.push(consultation);
        localStorage['consultations'] = JSON.stringify(consultations);
        commit('GET_CONSULTATIONS', consultations);
    },
    editConsultation({commit}, consultation) {
        let consultations = JSON.parse(localStorage.getItem("consultations"));

        consultations.forEach((consultationItem, index) => {
            if (consultationItem.id === consultation.id) {
                consultations[index] = consultation;
            }
        });

        localStorage['consultations'] = JSON.stringify(consultations);
        commit('GET_CONSULTATION', consultation);
    },
    deleteConsultation({commit}, consultationId) {
        let consultations = JSON.parse(localStorage.getItem("consultations"));

        consultations.forEach((consultationItem, index) => {
            if (consultationItem.id === consultationId) {
                consultations.splice(index, 1);
            }
        });

        localStorage['consultations'] = JSON.stringify(consultations);
    },
    moveConsultationModal({commit, state}, data) {
        state.modalConsultationType = data.type;
        state.modalConsultationState = data.state;
        state.modalConsultationId = data.id
    },
}

export default actions
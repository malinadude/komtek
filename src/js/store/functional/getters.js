let getters = {
    tableType: state => {
        return state.tableType;
    },
    tableHeadings: state => {
        return state.tableHeadings;
    },
    tableColumnDisplay: state => {
        return state.tableColumnDisplay;
    },
    tableFilterDisplay: state => {
        return state.tableFilterDisplay;
    },

    // Patient
    patient: state => {
        return state.patient;
    },
    patients: state => {
        return state.patients;
    },

    // Consultation
    consultation: state => {
        return state.consultation;
    },
    consultations: state => {
        return localStorage.getItem("consultations") ? JSON.parse(localStorage.getItem("consultations")) : state.consultations;
    },
    consultationsPatient: state => {
        return state.consultationsPatient;
    },
    modalConsultationType: state => {
        return state.modalConsultationType;
    },
    modalConsultationState: state => {
        return state.modalConsultationState;
    },
    modalConsultationId: state => {
        return state.modalConsultationId;
    }
}

export default  getters
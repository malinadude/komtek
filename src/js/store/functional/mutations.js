let mutations = {
    SET_TABLE_TYPE(state, type) {
        return state.tableType = type;
    },
    SET_TABLE_HEADINGS(state, tableHeadings) {
        return state.tableHeadings = tableHeadings;
    },
    SET_TABLE_COLUMN_DISPLAY(state, column) {
        return state.tableColumnDisplay.push(column);
    },

    // Patient
    GET_LIST_PATIENT(state, patients) {
        return state.patients = patients;
    },
    GET_PATIENT(state, patient) {
        return state.patient = patient;
    },
    CREATE_PATIENT(state, patient) {
        return state.patients.push(patient);
    },
    EDIT_PATIENT(state, patient) {
        return state.patient = patient;
    },

    // Consultation
    GET_CONSULTATIONS(state, consultations) {
        return state.consultations = consultations;
    },
    GET_CONSULTATIONS_PATIENT(state, consultationsPatient) {
        return state.consultationsPatient = consultationsPatient;
    },
    GET_CONSULTATION(state, consultation) {
        return state.consultation = consultation;
    },
    CREATE_CONSULTATION(state, consultation) {
        return state.consultations.push(consultation);
    },
    EDIT_CONSULTATION(state, consultation) {
        return state.consultation = consultation;
    },


    SET_FORM_ERRORS(state, errors) {
        return state.formErrors = errors;
    },
}

export default mutations

let state = {
    tableType: null,
    tableHeadings: null,
    tableColumnDisplay: [
        'id',
        'surname',
        'name',
        'patronymic',
        'birthday',
        'sex',
        'snils',
        'patient',
        'date',
        'time',
        'symptoms',
    ],
    tableFilterDisplay: {
        patients: {
            id: true,
            surname: true,
            name: true,
            patronymic: true,
            birthday: true,
            sex: true,
            snils: true,
        },
        consultations: {
            id: true,
            patient: true,
            date: true,
            time: true,
            symptoms: true,
        },
    },

    // Patient
    patient: null,
    patients: [],

    // Consultation
    consultation: null,
    consultations: null,
    consultationsPatient: null,
    modalConsultationType: '',
    modalConsultationState: false,
    modalConsultationId: null,
}

export default state
const mix = require('laravel-mix');

mix.options({
        processCssUrls: false,
        autoprefixer: {
            enabled: true,
            options: {
                browsers: ['last 2 versions', '> 1%'],
                cascade: true,
                grid: true,
            }
        }
    })
    .copyDirectory('src/fonts', 'public/fonts')
    .copyDirectory('src/images', 'public/images')
    .setPublicPath('public')
    .js('src/js/app.js', 'public/js')
    .extract(['vue'])
    .sass('src/sass/styles.scss', 'public/css')
    .version()
    .browserSync({
        proxy: false,
        server: {
            baseDir: './'
        },
        single: true
    });